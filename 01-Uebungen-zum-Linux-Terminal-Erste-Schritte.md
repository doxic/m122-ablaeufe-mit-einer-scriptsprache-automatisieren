# 01 - Übungen zum Linux-Terminal - Erste Schritte

Starten Sie in Ihrer Linux-Distribution ein Terminal. Um detaillierte Informa­tionen zu den angezeigten Befehlen zu kriegen, können Sie `man <befehl>` ausführen. `man` können Sie beenden mit `q`. Es werden jeweils nur neu hinzugekommene Befehle aufgelistet. Have fun! 

## Aktuelles Arbeitsverzeichnis 

Linux verfügt über keine Laufwerksbuchstaben wie Windows, sondern nur einen grossen Baum, der in / beginnt. In welchem Verzeichnis befinden Sie sich zur Zeit? 

```bash
pwd - print name of current/working directory 
```

## Unterverzeichnis erstellen 

Erstellen Sie mit nur einem Befehl folgendes Unterverzeichnis: uebung/start. Danach gerade auch uebung/inhalte, uebung/ausfuehren und uebung/loeschen. Zeigen Sie den Inhalt von uebung an.  

```bash
mkdir - make directories 

ls - list directory contents 
```

<!-- mkdir -p uebung/start uebung/inhalte uebung/ausfuehren uebung/loeschen -->

## Verzeichnisbäume anzeigen 

Zeigen sie den gesamten neu erstellten Baum an. 
```bash
find - search for files in a directory hierarchy 
```
<!-- find . -type d -->

## Dateisystem 

Wie Sie Daten auf ihrer Festplatte ablegen können, hängt vom verwen­de­ten Dateisystem ab (nicht etwa vom Betriebssystem!). Welches Datei­sys­tem wird denn für Ihr Unterzeichnis verwendet? Welche anderen Dateisys­teme wären möglich? 
```bash
mount - mount a file system 
```
<!--
mount
df -h
-->

## Sonderzeichen in Dateinamen 

Erstellen Sie im Verzeichnis uebung/start/ mittels `touch` die Datei  Vorname Nachname. Achten Sie auf den Leerschlag zwischen den Namen­kompo­nenten. Es gibt drei mögliche Varianten, wie Sie das bewerkstelligen. Welche? 
```bash
touch - change file timestamps 

bash - GNU Bourne-Again SHell (Abschnitt QUOTING) 
```
<!--
touch "Dominic Rüttimann"
touch 'Dominic Rütimann'
touch Dominic\ Rüttimann
-->

## Dateien und Verzeichnisse löschen 

Erstellen Sie in `uebung/loeschen` eine Datei `loesch_mich`. Versuchen Sie dann erst das Verzeichnis `uebung/loeschen` zu löschen, dann die Datei `loesch_mich`. Am Ende dieser Aufgabe sollten `uebung/loeschen` und `loesch_mich` entfernt sein. 
```bash
rm - remove files or directories 

rmdir - remove empty directories 
```
<!--
touch uebung/loeschen/loesch_mich
rm -rf touch uebung/loeschen
-->

## Dateien kopieren 

Kopieren Sie sich die Datei `/etc/passwd` ins Verzeichnis `uebung/inhalte/`. Diese Systemdatei enthält alle Benutzer und deren Angaben, welche für den Login nötig sind. 
```bash
cp - copy files and directories 
```
<!--
cp /etc/passwd uebung/inhalte/
-->

## Versteckte Dateien 

Wechseln Sie das aktuelle Arbeitsverzeichnis zu `uebung/inhalte` und benennen Sie die neu erstellte Kopie um in `.meine_user`. Achten Sie auf den Punkt am Anfang! Zeigen Sie die Dateien im aktuellen Verzeichnis an. Wo ist die Datei geblieben und wie können Sie sie trotz des Punkts wieder sichtbar machen? 
```bash
cd – change directory (Hilfe in `man bash` zu finden) 

ls - list directory contents 

mv - move (rename) files 
```
<!--
cd uebung/inhalte
mv passwd .meine_user
ls -a
-->

## Home Directory 

Kehren Sie zu Ihrem Home-Directory zurück. Wie gelangen Sie zu diesem, ohne sich zu Sorgen, mit welchem User Sie arbeiten? (mind. drei Möglichkeiten!)
<!--
cd
cd $HOME
cd ~
-->

## User-Konfiguration 

Zeigen Sie sich mal alle im Home Directory vorhandenen Daten an. Wozu sind diese gut? Beschreiben Sie die Dateien `.bash_*`. Schauen Sie sich ungeniert ein bisschen weiter in diesen Verzeichnissen um... 
```bash
less - opposite of more 

more - file perusal filter for crt viewing 
```



## Multiuser-System: User root 

Manche Daten müssen aus Sicherheitsgründen geheim gehalten sein, wie bspw. die Passwortdatei des Systems (`/etc/shadow`). Auch hat nur der Benutzer root Zugriff, um in Systemverzeichnisse schreiben zu können. So lässt sich vermei­den, dass Benutzer willkürlich Software ins System installieren. Zeigen Sie den Inhalt der Datei `/etc/shadow` an. 

```bash
sudo, sudoedit - execute a command as another user 
```

<!--
sudo cat /etc/shadow
-->

## Umgebungsvariablen 

Lassen Sie sich alle Umgebungs­variab­len Ihrer Shell anzeigen. 

(In neueren Distributionen werden diverse Funktionen in diesen Umgebungen abgelegt. Ignorieren Sie diese; die ersten Zeilen sind die herkömmlichen Umgebungsvariablen.) 

```bash
set (Suchen Sie in `man bash` mittels / nach “..set.\[“) 
```

## Spezifische Umgebungsvariablen anzeigen 

Mit `set` haben Sie gesehen, dass es eine Umgebungsvariable $HOME und $PATH gibt. Lassen Sie sich den Inhalt dieser Variable im Terminal anzeigen. Wozu sind diese gut? Welche anderen Umgebungs­variablen erscheinen Ihnen spannend? 

```bash
echo - display a line of text 
```

<!--
echo "HOME: $HOME\nPATH: $PATH"
-->

## Erstellen neuer Umgebungsvariablen 

Erstellen Sie eine neue Umgebungsvariable „me“ und speichern Sie darin Ihren Vor- und Nachnamen. Überprüfen Sie mit `echo`, ob’s funktionierte.

<!--
me="Dominic Rüttimann"
echo $me
-->

## Gültigkeitsbereich von UmgebungsvariablenI 

Starten Sie nun eine neue Shell (Aufruf von `bash`) und überprüfen Sie, ob „me“ darin existiert. Mit exit kommen Sie wieder raus. Ist die Umgebungsvariable noch vorhanden? Wie können Sie erreichen, dass die Umgebungsvariable auch in der Sub-Shell noch vorhanden ist?

```bash
bash - GNU Bourne-Again Shell 

export (Suchen Sie in `man bash` nach „..export.\[“) 
```

<!--
export me
-->

## Zeitzone 

In welcher Zeitzone ‚lebt‘ Ihr Rechner? Die Zeitzone ist nebenbei auch in /etc/timezone gespeichert. Zeigen Sie den Inhalt dieser Datei an. Versuchen Sie aber die Zeitzone auch mit dem Befehl `date` anzuzeigen.

```bash
date - print or set the system date and time 
```

## Aktive Taskliste 

Lassen Sie sich alle zur Zeit ausgeführten Tasks als Baum anzeigen. 

```bash
ps - report a snapshot of the current processes. 
```

<!--
ps -afx
-->

## Prozessüberwachung 

Welche Prozesse benutzen zurzeit viel CPU? Welche viel Memory? Sortieren Sie die Liste von `top` nach Memory-Verbrauch!

```bash
top - display Linux tasks 
```

<!--
top
<press SHIFT + m>
-->

## Aufzeichnen der Session 

Der Kunde möchte gerne im Nachhinein sehen, was Sie genau auf seinem Rechner machten. Dies zu Dokumentations- wie auch Absicherungszwecken.

```bash
script - make typescript of terminal session 
```

<!--
script file.txt
-->
