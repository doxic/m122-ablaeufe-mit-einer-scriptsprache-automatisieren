# Textbearbeitung

Starten Sie in Ihrer Linux-Distribution ein Terminal. Um detaillierte Informa­tionen zu den angezeigten Befehlen zu kriegen, können Sie `man <befehl>` ausführen. `man` können Sie beenden mit `q`. Es werden jeweils nur neu hinzugekommene Befehle aufgelistet. Have fun! 

## Filtern von Textdateien 

Erstellen Sie mit cp eine neue Kopie von `/etc/passwd`. Lassen Sie dann nur die Angaben zu Ihrem Benutzer anzeigen. 

```bash
grep, egrep, fgrep, rgrep - print lines matching a pattern 
```

<!-- 
cp /etc/passwd uebung/inhalte/ 
grep dominic passwd
-->

## Sortieren von Zeilen I 

Lassen Sie sich die Datei nach Benutzernamen sortiert anzeigen. 

```bash
sort - sort lines of text files 
```

<!-- 
sort passwd
-->

## Sortieren von Zeilen II 

Lassen Sie sich die Datei nach Gruppen-ID (4. Spalte) sortiert anzeigen. 

Siehe Befehl `sort`, Parameter `-t`, `-k` und `-n`

<!--        
sort -t: -k4 -n passwd
-t, --field-separator=SEP
use SEP instead of non-blank to blank transition
-k, --key=KEYDEF
sort via a key; KEYDEF gives location and type
-n, --numeric-sort
compare according to string numerical value 
-->

## Sotieren von Zeilen III 

Lassen Sie sich die Datei `passwd` nach Gruppen-ID absteigend sortiert anzeigen. 
<!--
sort -t: -k4 -nr passwd
-r, --reverse
reverse the result of comparisons
-->

## Dateien editieren 

Im Modul 100 haben Sie wohl eine Klassenliste erstellt. Übernehmen Sie aus dieser Liste alle Geburtsdaten im ISO-Format (yyyy-mm-dd). Schreiben Sie pro Mitschülerin und Mitschüler eine Zeile “yyyy-mm-dd nachname vorname”. Verwenden Sie dazu z.B. `kate`, `gedit`, `nano`, `joe` oder `vim`. Der Dateiname sei `meine_klasse.txt` 

```bash
nano - Nano’s ANOther editor, an enhanced free Pico clone 
joe - Joe’s Own Editor 
vim - Vi IMproved, a programmers text editor 
gedit - text editor for the GNOME Desktop 
kate - Advanced text editor for KDE 
```

<!--
cat << EOF > meine_klasse.txt
1990-12-10 pan peter
1991-11-09 muster han
1991-04-03 doe john
1995-01-23 bar foo
EOF
-->

## Dateien ausgeben 

Lassen Sie den Inhalt der neu erstellten Datei im Terminal ausgeben. 

```bash
cat - concatenate files and print on the standard output 
```

## Theorie Umleitung 

Mittels `|` (Je nach Keyboard-Layout Alt Gr + 7 oder Alt Gr + 1) können Sie den Output (`STDOUT`) eines Kommandos als Input für das nächste Statement verwenden: 

```bash
sort datei | more 
```

Mittels `>` lässt sich der Output (`STDOUT`) statt in Ihr Terminal in eine Datei umlenken: 

```bash
sort datei > /tmp/output 
```

Möglicherweise haben Sie den Output in eine Datei umgelenkt und möchten diese Datei nun in einem weiteren Befehl als `STDIN` weiterverarbeiten. Dies lässt sich mit `<` bewerkstelligen: 

```bash
sort < /tmp/output 
```

`>`ersetzt den bisherigen Inhalt der Datei. Gerade beim Logging (Aufzeichnen von Fehlermeldungen) möchten Sie die Datei mit dem neuen Output ergänzen. Dies machen Sie mit `>>`: 

```bash
echo fertig >> /tmp/output 
```

Nebst `STDIN` und `STDOUT` existiert für das Debugging und die Fehleranalyse auch der `STDERR`. Ein Kanal, der für Fehlermeldungen und Warnungen gebraucht wird. `2>` und `2>>` stehen für die Umleitung des `STDERR` zur Verfügung: 

```bash
find /etc > /tmp/output.ok 2> /tmp/output.error 
```

## Spalten 

Lassen Sie sich nur die Nach- und Vornamen anzeigen. 

```bash
cut - remove sections from each line of files 
```

<!--
cut -f 2,3 -d" " meine_klasse.txt
cut -f2- -d" " meine_klasse.txt
-->

## Kopf und Schwanz 

Geben Sie das jüngste und älteste Mitglied Ihrer Klasse ausgeben. Verwenden Sie dazu zwei Befehle. 

```bash
head - output the first part of files 

tail - output the last part of files 
```

<!--
sort meine_klasse.txt | head -n1
sort meine_klasse.txt | tail -n1
-->

## Filtern II 

Zeigen Sie alle Mitschülerinnen und Mitschüler, die im vierten Quartal Geburtstag haben in alphabetischer Reihenfolge an, ohne das Geburtsdatum zu erwähnen. 

<!--
grep '....-[1]' meine_klasse.txt | cut -f2- -d" " | sort
-->